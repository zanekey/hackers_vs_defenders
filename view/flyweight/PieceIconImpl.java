package view.flyweight;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PieceIconImpl implements PieceIcon {
    private Image iconImage;

    public PieceIconImpl(String imagePath) {
        try {
            File imgFile = new File(imagePath);
            this.iconImage = ImageIO.read(imgFile);
            this.iconImage = this.iconImage.getScaledInstance(35, 35, Image.SCALE_DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void drawImageAt(Graphics2D g, int x, int y) {
        g.drawImage(this.iconImage, x, y, null, null);
    }

    public Image getImage() {
        return this.iconImage;
    }
}
