package view.flyweight;

import java.awt.Graphics2D;
import java.awt.Image;

public interface PieceIcon {
    public abstract void drawImageAt(Graphics2D g, int x, int y);

    public abstract Image getImage();
}
