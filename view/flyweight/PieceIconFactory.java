package view.flyweight;

import java.util.HashMap;

public class PieceIconFactory {
    private static PieceIconFactory singleton;

    private HashMap<String, String> imagePathMap;
    private HashMap<String, PieceIcon> iconMap;

    private PieceIconFactory() {
        this.iconMap = new HashMap<String, PieceIcon>();

        //List of all image paths needed for game
        this.imagePathMap = new HashMap<String, String>();
        this.imagePathMap.put("UnpaidIntern", "images/unpaidIntern.png");
        this.imagePathMap.put("SecurityGuard", "images/securityGuard.png");
        this.imagePathMap.put("FirewallExpert", "images/firewallExpert.png");
        this.imagePathMap.put("DDOSPawn", "images/DDOSPawn.png");
        this.imagePathMap.put("DecryptionExpert", "images/DecryptionExpert.png");
        this.imagePathMap.put("Hacker", "images/hacker.png");
        this.imagePathMap.put("DefenderObjective", "images/objective.png");
    }

    public static PieceIconFactory getSingleton()    //singleton to prevent unneccessary creation and loading of imageicons
    {
        if (PieceIconFactory.singleton == null) {
            PieceIconFactory.singleton = new PieceIconFactory();
        }
        return PieceIconFactory.singleton;
    }

    public PieceIcon getIcon(String pieceName) {
        if (this.iconMap.keySet().contains(pieceName)) {
            return this.iconMap.get(pieceName);
        } else {
            PieceIcon newFlyweightIcon = new PieceIconImpl(this.imagePathMap.get(pieceName));
            this.iconMap.put(pieceName, newFlyweightIcon);
            return newFlyweightIcon;
        }
    }
}
