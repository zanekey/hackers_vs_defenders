package view;

import model.board.Board;
import model.board.PieceDetails;
import model.pieces.MoveablePiece;
import view.flyweight.PieceIcon;
import view.flyweight.PieceIconFactory;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class PieceStatus extends JPanel {

    JProgressBar healthBar;

    public PieceStatus(PieceDetails piece, String pieceType) {

        /*set layout */
        this.setLayout(new BorderLayout());

        healthBar = new JProgressBar();
        healthBar.setValue(piece.getHP());
        healthBar.setForeground(Color.GREEN);
        ImageIcon pieceIcon = new ImageIcon(getTierImage(piece, pieceType));
        JLabel pieceLabel = new JLabel(piece.getName(), pieceIcon, JLabel.LEFT);
        JLabel strengthLabel = new JLabel("Strength: " + Integer.toString(piece.getStrength()), null, JLabel.RIGHT);
        JLabel healthLabel = new JLabel("<html><font color='white'>health</font></html>", null, JLabel.RIGHT);

        JPanel placeHolderPanel = new JPanel(new GridLayout(1, 2));

//      pieceLabel.setBackground(Color.GRAY);
//      strengthLabel.setBackground(Color.ORANGE);
        /* add labels to place holder Panel , for organisational purposes */
        placeHolderPanel.add(pieceLabel);
        placeHolderPanel.add(strengthLabel);
        placeHolderPanel.setBackground(Color.GRAY);

        healthBar.setBackground(Color.RED);


        JPanel healthBarHolder = new JPanel(new BorderLayout());

        healthBarHolder.add(healthLabel, BorderLayout.WEST);
        healthBarHolder.add(healthBar, BorderLayout.CENTER);
        healthBarHolder.setBackground(Color.DARK_GRAY);

        healthBar.setBorderPainted(true);

        /* add to main frame */
        this.add(placeHolderPanel, BorderLayout.NORTH);
        this.add(healthBarHolder, BorderLayout.CENTER);


    }


    private Image getTierImage(PieceDetails piece, String pieceType) {
        int pieceIndex = piece.getTier() - 1;
        PieceIcon pieceIcon = PieceIconFactory.getSingleton().getIcon(piece.getName());
        return pieceIcon.getImage();
    }


    public void setHP(PieceDetails piece) {
        healthBar.setValue(piece.getHP());
    }
}