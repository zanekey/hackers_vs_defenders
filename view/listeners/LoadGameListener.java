package view.listeners;

import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import driver.SaveManager;

public class LoadGameListener implements ActionListener {
    private JFrame container;

    public LoadGameListener(JFrame container) {
        this.container = container;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            SaveManager.loadGame();
        } catch (FileNotFoundException e) {
            JDialog errorDialog = new JDialog(this.container, true);
            errorDialog.setSize(200, 100);
            errorDialog.add(new JLabel("Error: No save data detected.", SwingConstants.CENTER));
            errorDialog.setLocationRelativeTo(this.container);
            errorDialog.setVisible(true);
        } catch (IOException e) {
            JDialog errorDialog = new JDialog(this.container, true);
            errorDialog.setSize(200, 100);
            errorDialog.add(new JLabel("Error: Something went wrong with loading the data. File may be corrupted.", SwingConstants.CENTER));
            errorDialog.setLocationRelativeTo(this.container);
            errorDialog.setVisible(true);
        } catch (Exception e) {
            JDialog errorDialog = new JDialog(this.container, true);
            errorDialog.setSize(200, 100);
            errorDialog.add(new JLabel("Unknown error has occurred. Sorry!", SwingConstants.CENTER));
            errorDialog.setLocationRelativeTo(this.container);
            errorDialog.setVisible(true);
        }
    }
}
