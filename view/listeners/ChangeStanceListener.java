package view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import driver.GameController;
import model.pieces.states.Stance;

public class ChangeStanceListener implements ActionListener {
    private Stance stanceToChangeTo;

    public ChangeStanceListener(Stance stance) {
        this.stanceToChangeTo = stance;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        GameController.getSingleton().setSelectedPieceStance(this.stanceToChangeTo);
    }
}
