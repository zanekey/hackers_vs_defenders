package view.listeners;

import java.awt.event.*;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import driver.SaveManager;

public class SaveGameListener implements ActionListener {
    private JFrame container;

    public SaveGameListener(JFrame container) {
        this.container = container;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            SaveManager.saveGame();
        } catch (IOException e) {
            JDialog errorDialog = new JDialog(this.container, true);
            errorDialog.setSize(200, 100);
            System.err.println(e);
            errorDialog.add(new JLabel("Error: Something went wrong with saving the data. File may be corrupted.", SwingConstants.CENTER));
            errorDialog.setLocationRelativeTo(this.container);
            errorDialog.setVisible(true);
        } catch (Exception e) {
            JDialog errorDialog = new JDialog(this.container, true);
            errorDialog.setSize(200, 100);
            System.err.println(e);
            errorDialog.add(new JLabel("Unknown error has occurred. Sorry!", SwingConstants.CENTER));
            errorDialog.setLocationRelativeTo(this.container);
            errorDialog.setVisible(true);
        }
    }
}
