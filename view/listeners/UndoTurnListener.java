package view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import driver.GameController;
import view.MainFrame;

public class UndoTurnListener implements ActionListener {
    private int numTurnsToUndo;
    MainFrame mainAppFrame;

    public UndoTurnListener(MainFrame mainAppFrame, int turnsToUndo) {
        this.mainAppFrame = mainAppFrame;
        this.numTurnsToUndo = turnsToUndo;
    }

    public void actionPerformed(ActionEvent event) {
        if (GameController.getSingleton().activePlayerHasUndoLeft()) {
            GameController.getSingleton().undoCommand(this.numTurnsToUndo);
        } else {
            JDialog errorDialog = new JDialog(this.mainAppFrame, true);
            errorDialog.setLocationRelativeTo(this.mainAppFrame);
            JLabel errorMessage = new JLabel("You have already used your undo this game!", SwingConstants.CENTER);
            errorDialog.add(errorMessage);
            errorDialog.setSize(300, 100);
            errorDialog.setVisible(true);
        }
    }
}
