package view;

import java.awt.BorderLayout;

import javax.swing.*;

import model.pieces.states.*;
import view.listeners.ChangeStanceListener;

public class StancePanel extends JPanel {
    public StancePanel() {
        this.setLayout(new BorderLayout());
        JButton defensiveButton = new JButton("Defensive Stance");
        JButton balancedButton = new JButton("Balanced Stance");
        JButton offensiveButton = new JButton("Offensive Stance");

        defensiveButton.addActionListener(new ChangeStanceListener(DefensiveStance.getInstance()));
        balancedButton.addActionListener(new ChangeStanceListener(BalancedStance.getInstance()));
        offensiveButton.addActionListener(new ChangeStanceListener(OffensiveStance.getInstance()));

        this.add(defensiveButton, BorderLayout.WEST);
        this.add(balancedButton, BorderLayout.CENTER);
        this.add(offensiveButton, BorderLayout.EAST);
    }
}
