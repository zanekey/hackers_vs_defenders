package view;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

import driver.GameController;

public class GamePanel extends JPanel {

    private JButton endTurnButton = new JButton("End Turn");
    private JButton attackStateButton = new JButton("Change to Attack Mode");

    public GamePanel(GameMetaInformationPanel gameMetaInfoPanel) {
        setLayout(new GridLayout(4, 1));
        add(gameMetaInfoPanel);
        add(attackStateButton);
        this.add(new StancePanel());
        add(endTurnButton);


        attackStateButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (GameController.getSingleton().isAttackMode()) {
                    GameController.getSingleton().setAttackMode(false);
                    attackStateButton.setText("Change to Attack Mode");
                } else {
                    GameController.getSingleton().setAttackMode(true);
                    attackStateButton.setText("Change to Move Mode");
                }
            }
        });

        endTurnButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                GameController.getSingleton().endTurn();
            }
        });
    }

    public void updateAttackButton(boolean isAttackMode) {
        if (!isAttackMode) {
            attackStateButton.setText("Change to Attack Mode");
        } else {
            attackStateButton.setText("Change to Move Mode");
        }

    }
}
