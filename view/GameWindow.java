package view;

import javax.swing.*;

//import com.google.java.contract.Requires;

import java.awt.*;
import java.awt.event.*;

import model.ModelController;
import model.board.*;
import driver.GameController;

import java.util.HashSet;
import java.util.Set;

public class GameWindow extends JPanel {
    private final int WIDTH = 1200;
    private final int HEIGHT = 800;
    private final int HEX_RADIUS = 20;

    private BoardState currBoardState;

    public GameWindow() {
        this.setBackground(Color.black);

        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                processClick(evt);
            }
        });
    }

    public void processClick(MouseEvent actionEvent) {
        actionEvent.getComponent();
        int clickX = actionEvent.getX();
        int clickY = actionEvent.getY();
        boolean isAttackMode = GameController.getSingleton().isAttackMode();
        CubeCoordinate tile;

        if (isAttackMode)
            tile = matchClickToCoordinateIn(clickX, clickY, this.currBoardState.getAttackableCoordinates());
        else
            tile = matchClickToCoordinateIn(clickX, clickY, this.currBoardState.getLegalMoves());


        if (tile != null && !isAttackMode)//not attack mode
        {
            GameController.getSingleton().updateSelectedPiecePosition(tile);
        } else if (tile != null && isAttackMode)//is attack mode
        {
            if (tile.equals(new CubeCoordinate(0, 0, 0))) //the base is hit
            {
                GameController.getSingleton().applyDamageToBase();
            } else {
                int defendingPieceID;
                tile = this.matchClickToCoordinateIn(clickX, clickY, this.currBoardState.getAllPlayerPieceCoordinates());
                defendingPieceID = this.currBoardState.getPieceIDAt(tile);
                GameController.getSingleton().applyDamageTo(defendingPieceID);
            }
        } else {
            if (ModelController.getSingleton().getActivePlayer() == 0)
                tile = this.matchClickToCoordinateIn(clickX, clickY, this.currBoardState.getHackerPieceCoordinates());
            else
                tile = this.matchClickToCoordinateIn(clickX, clickY, this.currBoardState.getDefenderPieceCoordinates());
            int pieceID = this.currBoardState.getPieceIDAt(tile);
            GameController.getSingleton().setSelectedPiece(pieceID);
        }
    }

    // function to retrieve position of mouse click and determine the hex that has been clicked
    private CubeCoordinate matchClickToCoordinateIn(int clickX, int clickY, Set<CubeCoordinate> setInFocus) {
        for (CubeCoordinate c : setInFocus) {
            int[] axialCoordinates = getPixelCoordinate(c);
            int pieceX = axialCoordinates[0];
            int pieceY = axialCoordinates[1];

            /* bounds for determining clicked on Hex. */
            int westBound = pieceX + HEX_RADIUS;
            int eastBound = pieceX - HEX_RADIUS;
            int northBound = pieceY + HEX_RADIUS;
            int southBound = pieceY - HEX_RADIUS;

            /* conditions that must be satisfied */
            Boolean withinXBound = clickX > eastBound && clickX < westBound;
            Boolean withinYbound = clickY > southBound && clickY < northBound;

            /* return found hex */
            if (withinXBound && withinYbound) {
                return c;
            }
        }
        return null;
    }

    // convert cube Coordinates to pixel coordinates
    private int[] getPixelCoordinate(CubeCoordinate coordinate) {
        // empty array to hold pixel coordinates;
        int[] pixelCoordinates = new int[2];
        // get axial coordinates of current CubeCoordinate
        int[] axialCoordinates = coordinate.toAxialCoordinates();
        int xCoordinate = axialCoordinates[0];
        int yCoordinate = axialCoordinates[1];

        xCoordinate = (int) ((WIDTH / 2 - 150) + (xCoordinate * (HEX_RADIUS * 2)) + (yCoordinate * (HEX_RADIUS)));
        yCoordinate = (int) ((HEIGHT / 2 - 25) + ((yCoordinate * (HEX_RADIUS))) + (yCoordinate * ((2 * HEX_RADIUS) / 3)));
        pixelCoordinates[0] = xCoordinate;
        pixelCoordinates[1] = yCoordinate;

        return pixelCoordinates;
    }

    // @Requires("currBoardState != null")
    public void updateView(BoardState currBoardState) {
        this.currBoardState = currBoardState;
        this.repaint();
    }

    // paint component which paints the grid and any updates through reprint //
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        drawBoard(g);
    }

    // printing handler
    private void drawBoard(Graphics g) {
        Set<CubeCoordinate> tempSet = new HashSet<CubeCoordinate>();
        tempSet.add(this.currBoardState.getObjectivePosition());

        this.drawHexes(g, this.currBoardState.getBoardTiles(), 0x606060, false);
        this.drawHexes(g, this.currBoardState.getObstacles(), 0XE2C966, true);
        this.drawHexes(g, this.currBoardState.getHackerPieceCoordinates(), 0xFFFF0000, true);
        this.drawHexes(g, this.currBoardState.getDefenderPieceCoordinates(), 0xFF0000FF, true);
        this.drawHexes(g, tempSet, 0xffb100, true);
        this.drawHexes(g, this.currBoardState.getLegalMoves(), 0xFF777777, true);
        this.drawHexes(g, this.currBoardState.getAttackableCoordinates(), 0xF35CE9, false);
        this.drawHexes(g, this.currBoardState.getHackerPieceCoordinates(), 0xFFFF0000, true);
        this.drawHexes(g, this.currBoardState.getDefenderPieceCoordinates(), 0xFF0000FF, true);

        this.drawHexes(g, this.currBoardState.getLegalMoves(), 0xFF777777, true);
        this.drawHexes(g, this.currBoardState.getAttackableCoordinates(), 0xFF0000, true);
        this.drawImageHexes(g, this.currBoardState.getDefenderPieces());
        this.drawImageHexes(g, this.currBoardState.getHackerPieces());
        this.drawBase(g, this.currBoardState.getObjectivePosition());
    }

    public void drawHexes(Graphics g, Set<CubeCoordinate> hexes, int colour, boolean filled) {
        for (CubeCoordinate hex : hexes) {
            int pixelCoordinates[] = this.getPixelCoordinate(hex);
            int x = pixelCoordinates[0];
            int y = pixelCoordinates[1];
            this.drawHex(g, x, y, colour, filled);
        }
    }

    // convert cube Coordinates to pixel coordinates
    private void drawHex(Graphics g, int x, int y, int colour, boolean filled) {
        Graphics2D g2d = (Graphics2D) g;
        GraphicalHex hex = new GraphicalHex(x, y, HEX_RADIUS);
        // draw hex
        hex.draw(g2d, x, y, 5, colour, filled);
        hex.draw(g2d, x, y, 2, 0x00ff00, false);
    }


    /********* IMAGE DRAWING **********/
    public void drawImageHexes(Graphics g, Set<PieceDetails> pieces) {
        for (PieceDetails piece : pieces) {
            int pixelCoordinates[] = this.getPixelCoordinate(piece.getPosition());
            int x = pixelCoordinates[0];
            int y = pixelCoordinates[1];
            this.drawImageHex(g, x, y, piece);
        }
    }

    private void drawImageHex(Graphics g, int x, int y, PieceDetails piece) {
        Graphics2D g2d = (Graphics2D) g;
        GraphicalHex hex = new GraphicalHex(x, y, HEX_RADIUS);
        // draw hex
        hex.drawImage(g2d, x, y, piece);
        hex.drawImage(g2d, x, y, piece);
    }


    private void drawBase(Graphics g, CubeCoordinate objectivePosition) {
        int pixelCoordinates[] = this.getPixelCoordinate(objectivePosition);
        int x = pixelCoordinates[0];
        int y = pixelCoordinates[1];

        Graphics2D g2d = (Graphics2D) g;
        GraphicalHex hex = new GraphicalHex(x, y, HEX_RADIUS);

        hex.drawBase(g2d, x, y);
    }
}
