package view;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import model.board.BoardState;

public class TeamPieceDisplay extends JPanel {
    JTabbedPane teamPiecesTab;
    GameInfoDisplay defenderPieces, hackerPieces;

    public TeamPieceDisplay(BoardState boardState) {

        this.setLayout(new GridLayout(1, 1));

        teamPiecesTab = new JTabbedPane();
        defenderPieces = new GameInfoDisplay(boardState.getDefenderPieces(), "defender");
        hackerPieces = new GameInfoDisplay(boardState.getHackerPieces(), "hacker");


        teamPiecesTab.addTab("Hackers", hackerPieces);
        teamPiecesTab.addTab("Defenders", defenderPieces);

        this.add(teamPiecesTab);
        this.setVisible(true);


    }

    public void setActivePlayer(int activePlayer) {
        teamPiecesTab.setSelectedIndex(activePlayer);
    }
}
