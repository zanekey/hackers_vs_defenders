package view;


import model.board.BoardState;
import model.board.PieceDetails;
import model.pieces.MoveablePiece;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class GameInfoDisplay extends JPanel {

    private BoardState currBoardState;
    private PieceStatus pieceStatus;

    public GameInfoDisplay(Set<PieceDetails> setPieces, String pieceType) {
        this.setLayout(new GridLayout(6, 1));

        ArrayList<PieceDetails> allPieces = new ArrayList<PieceDetails>(setPieces);
        Collections.sort(allPieces, new Comparator<PieceDetails>() {
            @Override
            public int compare(PieceDetails o1, PieceDetails o2) {
                return o1.getID() - o2.getID();
            }
        });
        for (PieceDetails piece : allPieces) {

            add(new PieceStatus(piece, pieceType));

        }
    }

}