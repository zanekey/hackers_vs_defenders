package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import view.listeners.LoadGameListener;
import view.listeners.SaveGameListener;
import view.listeners.UndoTurnListener;

public class GameMenuBar extends JMenuBar {

    /* instantaite fields */
    private JMenu menu = new JMenu("File");
    private JMenu undo = new JMenu("Undo");
    private JMenuItem undo1 = new JMenuItem("1 turn");
    private JMenuItem undo2 = new JMenuItem("2 turns");
    private JMenuItem undo3 = new JMenuItem("3 turns");
    private JMenuItem load = new JMenuItem("Load");
    private JMenuItem save = new JMenuItem("Save");

    /* menu bar */
    public GameMenuBar(MainFrame mainFrame) {
        undo1.addActionListener(new UndoTurnListener(mainFrame, 1));
        undo2.addActionListener(new UndoTurnListener(mainFrame, 2));
        undo3.addActionListener(new UndoTurnListener(mainFrame, 3));
        load.addActionListener(new LoadGameListener(mainFrame));
        save.addActionListener(new SaveGameListener(mainFrame));

        undo.add(undo1);
        undo.add(undo2);
        undo.add(undo3);

        menu.add(undo);
        menu.add(load);
        menu.add(save);

        this.add(menu);
    }
}
