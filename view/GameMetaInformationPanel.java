package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GameMetaInformationPanel extends JPanel {


    private JLabel lblTimer, lblPlayer;
    private int timer;

    public GameMetaInformationPanel() {
        setLayout(new GridLayout(2, 1));
        setBackground(Color.RED);

        lblTimer = new JLabel(String.valueOf(timer), SwingConstants.CENTER);
        lblPlayer = new JLabel("Current Player: Hacker", SwingConstants.CENTER);


        lblPlayer.setFont(new Font("Dialog", Font.BOLD, 20));
        lblPlayer.setForeground(Color.white);

        lblTimer.setFont(new Font("Dialog", Font.BOLD, 50));
        lblTimer.setBackground(Color.black);
        lblTimer.setOpaque(true);

        add(lblTimer);
        add(lblPlayer);

    }

    public void setTimer(int interval) {
        this.timer = interval;
        this.lblTimer.setText(String.valueOf(interval));
        if (interval > 15)
            this.lblTimer.setForeground(Color.green);
        else if (interval < 15) {
            this.lblTimer.setForeground(Color.orange);
            if (interval <= 5)
                this.lblTimer.setForeground(Color.red);
        }
    }

    public void setLabelPlayer(int activePlayer) {
        if (activePlayer == 0) {
            this.setBackground(Color.RED);
            lblPlayer.setText("Current Player: Hacker");
        } else {
            this.setBackground(Color.BLUE);
            lblPlayer.setText("Current Player: Defender");
        }
    }
}
