package view;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import model.board.BoardState;

import javax.swing.*;

import driver.GameController;
import view.listeners.LoadGameListener;
import view.listeners.SaveGameListener;
import view.listeners.UndoTurnListener;

public class MainFrame extends JFrame {
    private GameWindow gameWindow;

    public GameMetaInformationPanel gameMetaInfoPanel;
    public GameSidePanel gameSidePanel;

    public MainFrame(String title, BoardState initialState) {
        super(title);
        this.setLayout(new BorderLayout());
        this.gameWindow = new GameWindow();
        this.gameWindow.updateView(initialState);

        /* add menu bar */
        this.setJMenuBar(new GameMenuBar(this));

        /* game Side Panel */
        gameSidePanel = new GameSidePanel(initialState);

        /* add panels */
        this.add(gameWindow, BorderLayout.CENTER);
        this.add(gameSidePanel, BorderLayout.EAST);

        /* frame settings */
        this.setSize(1350, 800);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
        this.setBackground(Color.black);
    }

    public void updateView(BoardState boardState, boolean isAttackMode) {
        this.gameWindow.updateView(boardState);
        gameSidePanel.updateView(boardState, isAttackMode);
    }

    public void setActivePlayer(int activePlayer) {
        gameSidePanel.teamPieceDisplay.setActivePlayer(activePlayer);
        gameSidePanel.gameMetaInfoPanel.setLabelPlayer(activePlayer);
    }

    public void displayWinner(String string) {
        JDialog winDialog = new JDialog(this, true);
        winDialog.setLocationRelativeTo(this);
        JLabel errorMessage = new JLabel("The " + string + " have won the game!", SwingConstants.CENTER);
        winDialog.add(errorMessage);
        winDialog.setSize(300, 100);
        winDialog.setVisible(true);
    }
}
