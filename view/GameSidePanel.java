package view;

import java.awt.GridLayout;

import javax.swing.JPanel;

import model.board.BoardState;

public class GameSidePanel extends JPanel {

    public TeamPieceDisplay teamPieceDisplay;
    public GameMetaInformationPanel gameMetaInfoPanel;
    private GamePanel gamePanel;
    JPanel tempPanel;

    public GameSidePanel(BoardState initialState) {
        setLayout(new GridLayout(2, 1));
        this.tempPanel = new JPanel(new GridLayout(1, 1));
        teamPieceDisplay = new TeamPieceDisplay(initialState);
        tempPanel.add(teamPieceDisplay);
        gameMetaInfoPanel = new GameMetaInformationPanel();
        gamePanel = new GamePanel(gameMetaInfoPanel);

        add(tempPanel);
        add(gamePanel);
    }


    public void updateView(BoardState boardState, boolean isAttackMode) {
        int index = teamPieceDisplay.teamPiecesTab.getSelectedIndex();
        tempPanel.remove(this.teamPieceDisplay);
        this.teamPieceDisplay = new TeamPieceDisplay(boardState);
        teamPieceDisplay.teamPiecesTab.setSelectedIndex(index);
        tempPanel.add(teamPieceDisplay);
        this.gamePanel.updateAttackButton(isAttackMode);
    }
}
