package driver;

import java.io.Serializable;
import java.util.Stack;

import model.commands.Command;
import model.commands.EndTurnCommand;

public class HistoryManager implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8830502697748143692L;
    private Stack<Command> commandHistory;

    public HistoryManager() {
        this.commandHistory = new Stack<Command>();
    }

    public void storeAndExecute(Command command) {
        command.execute();
        this.commandHistory.push(command);
    }

    /**
     * mechanism for undo, method will undo game play rounds and all moves inside each round
     *
     * @param turnsToUndo specifies the stopping condiiton for undoing
     *                    undo is achived through pop() of command history stack
     */
    public void undoCommand(int turnsToUndo) {
        int numEndTurnCommands = 0;
        while (true && !this.commandHistory.isEmpty()) {
            Command currCommand = this.commandHistory.pop();
            currCommand.undo();
            if (currCommand instanceof EndTurnCommand) {
                numEndTurnCommands++;
                if (numEndTurnCommands > turnsToUndo * 2) //need to undo both players last x turns
                {
                    this.commandHistory.push(currCommand); //replace the last endturn command onto the stack, as we did not revert that turn
                    break;
                }
            }
        }
    }
}
