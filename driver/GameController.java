package driver;

import model.ModelController;

import model.board.CubeCoordinate;
import model.board.PieceDetails;
import model.commands.AttackCommand;
import model.commands.BaseAttackCommand;
import model.commands.Command;
import model.commands.EndTurnCommand;
import model.commands.MoveCommand;
import model.pieces.states.Stance;
import model.board.BoardState;
import view.MainFrame;

import java.io.*;
import java.util.Arrays;
import java.util.Stack;

public class GameController implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1045852308040226322L;
    private static GameController singleton;
    private ModelController modelController;
    private MainFrame view;
    private HistoryManager historyManager;
    private int selectedPieceID;
    private boolean isAttackMode;

    /**
     * Model & game initialisation, frame is created and boardstate added
     */
    private GameController() {
        this.modelController = ModelController.getSingleton();
        this.historyManager = new HistoryManager();
        this.selectedPieceID = -1;
        this.view = new MainFrame("OOSD Assignment 2 | null", this.modelController.getBoardState(this.selectedPieceID, this.isAttackMode));
    }

    /**
     * Last instantiation of Singleton object
     *
     * @return GameController.singleton
     */
    public static GameController getSingleton() {
        if (GameController.singleton == null) {
            GameController.singleton = new GameController();
        }
        return GameController.singleton;
    }

    /**
     * called by LoadCommand
     * Sets the GameController data with the param passed in
     *
     * @param game
     */
    public void loadGame(GameController game) {
        GameController.singleton = game;
        GameController.singleton.view = this.view; //Transfer over current view to loaded controller
        ModelController.setSingleton(game.modelController);
        GameController.singleton.updateView();
    }

    public void startTimer() {
        modelController.startTimer();
    }

    /**
     * Updates the current selected piece in model, updates view.
     *
     * @param selectedPieceID
     */
    public void setSelectedPiece(int selectedPieceID) {
        this.selectedPieceID = selectedPieceID;
        this.updateView();
    }

    /**
     * Updates the displayed location of a piece when called by moveCommand
     * push' move command to stack
     * updates view
     *
     * @param newPosition
     */
    public void updateSelectedPiecePosition(CubeCoordinate newPosition) {
        BoardState currBoardState = this.modelController.getBoardState(this.selectedPieceID, this.isAttackMode);
        PieceDetails selectedPieceDetails = currBoardState.getDetailsForPieceWithID(this.selectedPieceID);
        Command moveCommand = new MoveCommand(selectedPieceDetails, this.selectedPieceID, newPosition);

        moveCommand.execute();
        this.historyManager.storeAndExecute(moveCommand);
        this.setAttackMode(true);

        this.updateView();
    }

    /**
     * creates a new object of boardstate with the current selected piece and the state of attackMode,
     * pushes object to view.
     */
    private void updateView() {
        BoardState boardState = this.modelController.getBoardState(this.selectedPieceID, this.isAttackMode);
        this.view.updateView(boardState, this.isAttackMode);
    }

    /**
     * called by controller to update timer displayed on view.
     *
     * @param interval
     */
    public void setTimer(int interval) {
        view.gameSidePanel.gameMetaInfoPanel.setTimer(interval);
    }

    /**
     * called by controller to update current players turn displayed on view.
     *
     * @param activePlayer
     */
    public void setActivePlayer(int activePlayer) {
        view.setActivePlayer(activePlayer);
    }

    /**
     * called by end turn button on view or when play conditions have been exhausted.
     * ends turn for current player by calling endTurnCommand and sets environment for new player.
     */
    public void endTurn() {
        Command endTurnCommand = new EndTurnCommand();
        this.historyManager.storeAndExecute(endTurnCommand);
        this.setAttackMode(false);
    }

    /**
     * called when user clicks a tile when attackmode == true
     * if pieceID is a GamePiece a new Boardstate object is created, attackCommand executes
     * and view is updated
     *
     * @param defendingPieceID
     */
    public void applyDamageTo(int defendingPieceID) {
        if (defendingPieceID != -1) {
            BoardState currBoardState = this.modelController.getBoardState(this.selectedPieceID, this.isAttackMode);
            PieceDetails defendingPieceDetails = currBoardState.getDetailsForPieceWithID(defendingPieceID);
            Command attackCommand = new AttackCommand(defendingPieceDetails, this.selectedPieceID, defendingPieceID);
            attackCommand.execute();
            this.historyManager.storeAndExecute(attackCommand);
        }
        this.selectedPieceID = -1;
        this.endTurn();
        this.updateView();
    }

    /**
     * called when user clicks the DefenderObjective tile when attackmode == true
     * baseattackCommand executes and view is updated
     */
    public void applyDamageToBase() {
        BaseAttackCommand baseAttackCommand = new BaseAttackCommand();
        this.historyManager.storeAndExecute(baseAttackCommand);
        this.selectedPieceID = -1;
        this.endTurn();
        this.updateView();
    }

    public boolean isAttackMode() {
        return this.isAttackMode;
    }

    public void setAttackMode(boolean mode) {
        this.isAttackMode = mode;
        this.updateView();
    }

    /**
     * checks if player can undo
     *
     * @return false if not.
     */
    public boolean activePlayerHasUndoLeft() {
        return this.modelController.activePlayerHasUndoLeft();
    }

    /**
     * Mechanism to undo x number of turns
     * while the commandlist stack is not empty, the pop command is run until the number of requested undo' has been fulfilled
     *
     * @param turnsToUndo
     */
    public void undoCommand(int turnsToUndo) {
        this.historyManager.undoCommand(turnsToUndo);
        this.selectedPieceID = -1;    //unset any selected piece
        this.updateView();
    }

    /**
     * method is called as a stopping condition for the current game.
     * if 30 turns have been played or the modelcontroller returns the active player == 1, defenders win dialog box is displayed
     * otherwise hacker win dialog is shown and game exits on close.
     *
     * @param turnNumber
     */
    public void displayWinner(int turnNumber) {
        if (turnNumber > 30 || this.modelController.getActivePlayer() == 1) {
            this.view.displayWinner("Defenders");
        } else {
            this.view.displayWinner("Hackers");
        }
        System.exit(0);
    }

    /**
     * Controls stance change with model and view.
     *
     * @param stanceToChangeTo
     */
    public void setSelectedPieceStance(Stance stanceToChangeTo) {
        if (this.selectedPieceID != -1) {
            this.modelController.setStanceForPieceWithID(this.selectedPieceID, stanceToChangeTo);
            this.updateView();
        }
    }
}