package driver;

public class Driver {
    public static void main(String[] args) {
        GameController game = GameController.getSingleton();
        game.startTimer();
    }
}
