package driver;

import java.io.*;

public class SaveManager {
    private final static String saveDataPath = "saveData.dat";

    /**
     * Saves GameController Singleton in its current state to file
     *
     * @throws IOException
     */
    public static void saveGame() throws IOException {
        FileOutputStream outFile = new FileOutputStream(saveDataPath);
        ObjectOutputStream outStream = new ObjectOutputStream(outFile);
        outStream.writeObject(GameController.getSingleton());
        outStream.close();
    }

    /**
     * loads game from file via GameController.loadGame method
     *
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void loadGame() throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream inFile = new FileInputStream(saveDataPath);
        ObjectInputStream inStream = new ObjectInputStream(inFile);
        GameController game = (GameController) inStream.readObject(); //Guaranteed to be a GameController as this is the only thing written to file
        GameController.getSingleton().loadGame(game);
        inStream.close();
    }
}
