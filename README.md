# Hackers vs Defenders #

Group project consisting of 4 members that were tasked in creating a multiplayer game that is developed in Java with a strong emphasis on the SOLID and GRASP object orientated design principles. In addition, the project must incorporate several design patterns 

###  Single Responsibility ###

	The Single responsibility principle states that every class should have accountability over a
	single part of the functionality required by the software, and that control should be entirely
	encapsulated by the class. Our Board class, shown through diagrams attached, is only
	concerned with holding information about the board state itself. This Cohesive design
	ensures the Board does not rely on other components of the software to complete its role of
	maintaining the parameters of the Board. Coupled with other utility classes like
	movementHelper, in which it delegates any pathfinding logic to is another example of a
	class maintaining a single responsibility.
	
### Open/Close Principle ###

	Open closed principle, shown in our class diagram, through inheritance hierarchy for player
	Pieces. getAttackableCoordinates is abstract and will need to be implemented for each
	specialised class.To obtain the attackable coordinates for a unit, we will simply call
	getAttackableCoordinates on a MoveablePiece.
	This use of polymorphism allows us add new specialised unit types without changing the
	existing code to accommodate the extension. Thus, the piece system is open for extension,
	but closed for modification.

### Controller ###

	The controller pattern assigns the responsibility of dealing with system events to a non-UI
	class that represents the overall system, ModelController shows the controller pattern in
	that it simply delegates down to appropriate model classes for each operation, acting as an
	interface for the overall GameController to query model data. It Does minimal logical work
	itself, only keeping track of turn number and turn length.
	Please also note the implementation of model-view-controller pattern as shown through
	GameController (overall controller) holding references to both the view and model, and
	acting as a communicator between the two.

### Information expert ###

	Information expert, shown in both board and movementhelper class, Board is an expert
	in the state of the board but has no idea about how to move around itself. Instead,
	movementHelper, which knows nothing about the board itself except for what is passed to
	it, is an expert for movement of pieces over the board and connectivity of tiles.
	Similarly, the moveablePieces are experts on what they are capable of, and so we would
	call to them if we wanted to know how what tiles they can attack, once this functionality is
	implemented.

### UML Diagram ###
#### Diagram 1 ####
![umldiagram1](images/demo/uml_diagram_1.PNG uml diagram 1)

#### Diagram 2 ####
![umldiagram 2](images/demo/uml_diagram_2.PNG uml diagram 2)
	
### Use case Diagram ###
![usecasediagram](images/demo/use_case_diagram.PNG use case diagram)

### Game Instructions
#### Instructions ####
 * Each player has 7 pieces of 3 types. 4 of weakest, 2 of middle, 1 of strongest.
 * Game ends when attacking team hacks the base, or defending team survives for x turns. x can be set before starting the game.
 * Each piece has 100hp, represented as a percentage
 * Each player gets 3 actions per turn, which can be spent moving any of their units, or attacking with any of their units. attack costs 1, move costs 1.
 * Default turn timer of 30 seconds per player action point. If 30 seconds passes, player loses that action point. Timer resets and they can spend their remaining points. 
 * Turn timer and board conﬁguration are customisable.

#### Red Team ####
 Red team is a team of white hat hackers hired by an organisation to test out their cyber security defences.
 red team objective = Hack the server or Defeat all opponents.
 
 ![red team stats](images/demo/red_team.PNG red team stats)
 
#### Blue Team ####
 Blue team is a team made up of cyber security personal hired by the company to protect against cyber threats.
 Blue team objective = Defend the server or Defeat all opponents.
 
 ![blue team stats](images/demo/blue_team.PNG blue team stats)
 
### Game Images and Demo ###

#### Different stances Implemented ####
![stances](images/demo/different_stances.gif different stances)
	
#### Attack patterns ####
![stances](images/demo/atatck_patterns.gif different stances)

### Contribution guidelines ###
	*Benjamin W
	*Elliot J
 	*Zeeshan M
 	*Zane K
