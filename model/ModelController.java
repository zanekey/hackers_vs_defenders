package model;

import driver.GameController;
import model.board.CubeCoordinate;
import model.board.PieceDetails;
import model.pieces.states.Stance;
import model.board.Board;
import model.board.BoardState;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

//import com.google.java.contract.Ensures;
//import com.google.java.contract.Invariant;

//@Invariant("this.activePlayer == 0 || this.activePlayer == 1")
public class ModelController implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4674824617483999785L;
    private static ModelController singleton;
    private static Timer timer;
    private boolean hasMovedThisTurn;
    private Board board;
    private int activePlayer;
    private boolean hasUsedUndo[];
    private int turnNumber;
    private int interval;

    /**
     * Sets the initial state of the game including boardsize
     *
     * @param boardRadius
     */
    private ModelController(int boardRadius) {
        this.board = new Board(boardRadius);
        this.turnNumber = 1;
        this.activePlayer = 0;
        this.hasUsedUndo = new boolean[2];
        Arrays.fill(this.hasUsedUndo, false);
        this.hasMovedThisTurn = false;
    }

    /**
     * Last instantiation of Singleton object
     *
     * @return ModelController.singleton
     */
    public static ModelController getSingleton() {
        if (ModelController.singleton == null) {
            ModelController.singleton = new ModelController(10);
        }
        return ModelController.singleton;
    }

    /**
     * Sets the data of the modelController object
     * this is safe because a new modelcontroller can only possibly be loaded from file, should not end up with unintended consequences
     * Called by GameController
     *
     * @param modelController
     */
    public static void setSingleton(ModelController modelController) {
        ModelController.singleton = modelController;
    }

    /**
     * @param selectedPieceID
     * @param isAttackMode
     * @return oardState object with the current active player selected piece.
     * result changes dependent on attack mode or move mode
     */
    public BoardState getBoardState(int selectedPieceID, boolean isAttackMode) {
        return this.board.getBoardState(selectedPieceID, this.activePlayer, this.hasMovedThisTurn, isAttackMode);
    }

    /**
     * starts game timer length, interval and calls interval loop
     * interval is posted to view to display time to user
     */
    public void startTimer() {
        int secs = 30;
        int delay = 1000;
        int period = 1000;
        timer = new Timer();
        interval = secs;
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                setInterval();
                postInterval(interval);
            }
        }, delay, period);
    }

    private int setInterval() {
        if (interval == 0) {
            this.swapTurn();
        }
        return --interval;
    }

    //@Ensures("this.turnNumber == old(this.turnNumber) + 1")
    public void swapTurn() {
        this.turnNumber++;
        if (this.gameEnded()) {
            GameController.getSingleton().displayWinner(this.turnNumber);
        }
        this.activePlayer = (this.activePlayer + 1) % 2;
        GameController.getSingleton().setSelectedPiece(-1);

        timer.cancel();
        startTimer();
        postActivePlayer(this.activePlayer);
        this.hasMovedThisTurn = false;
    }

    /**
     * A single player is able to undo only ONCE per game this class identifies if the current player is able to undo
     *
     * @return true if the player is able to undo
     * @retrun false if the player is unable to
     */
    public boolean activePlayerHasUndoLeft() {
        if (this.hasUsedUndo[this.activePlayer]) {
            return false;
        } else {
            this.hasUsedUndo[this.activePlayer] = true;
            return true;
        }
    }

    /**
     * Undo turn mechanism
     */
    public void unwindTurn() {
        this.activePlayer = (this.activePlayer + 1) % 2;
        this.turnNumber--;
        GameController.getSingleton().setSelectedPiece(-1);
        timer.cancel();
        startTimer();
        postActivePlayer(activePlayer);
    }

    private boolean gameEnded() {
        if (this.turnNumber > 30 || board.allDefendersDead() || board.allHackersDead() || board.baseDestroyed()) //if both players have had 15 turns
        {
            return true;
        }
        return false;
    }

    /**
     * Called by MoveCommand to change a selected piece' position and prevent user from moving again until next turn
     *
     * @param pieceID
     * @param newPosition
     */
    public void updatePiecePosition(int pieceID, CubeCoordinate newPosition) {
        this.board.updatePiecePosition(pieceID, newPosition);
        this.hasMovedThisTurn = true;
    }

    /**
     * Called by AttackCommand to apply damage to an attacked piece
     *
     * @param attackingPieceID
     * @param pieceBeingAttackedID
     */
    public void applyAttack(int attackingPieceID, int pieceBeingAttackedID) {
        this.board.applyAttack(attackingPieceID, pieceBeingAttackedID);
    }


    /**
     * posts timer interval to view to display timer to user
     *
     * @param interval
     */
    public void postInterval(int interval) {
        GameController.getSingleton().setTimer(interval);
    }

    /**
     * posts the active player to various view components
     *
     * @param activePlayer
     */
    public void postActivePlayer(int activePlayer) {
        GameController.getSingleton().setActivePlayer(activePlayer);
    }

    /**
     * Used by undo command to restore the piece details
     *
     * @param pieceBeforeCommand
     */
    public void restoreState(PieceDetails pieceBeforeCommand) {
        board.restoreState(pieceBeforeCommand);
    }

    public int getActivePlayer() {
        return activePlayer;
    }

    public void applyDamageToBase() {
        this.board.applyDamageToBase();
    }

    public void healBase() {
        this.board.healBase();
    }

    /**
     * used by state pattern to allow pieces to select various movement/attack stance
     *
     * @param selectedPieceID
     * @param stanceToChangeTo
     */
    public void setStanceForPieceWithID(int selectedPieceID, Stance stanceToChangeTo) {
        if (!this.hasMovedThisTurn) //only allow stance change before moving
        {
            this.board.setStanceForPieceWithID(selectedPieceID, stanceToChangeTo);
        }

    }
}
