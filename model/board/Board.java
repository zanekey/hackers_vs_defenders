package model.board;

import java.util.Set;
import java.io.Serializable;

//import com.google.java.contract.Ensures;
//import com.google.java.contract.Requires;

import java.util.HashSet;

import driver.GameController;
import model.pieces.*;
import model.pieces.defenders.DefenderObjective;
import model.pieces.states.Stance;

public class Board implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1781983095592417610L;
    private final int BOARD_RADIUS;
    private Set<CubeCoordinate> boardTiles;
    private Set<Obstacle> obstacles;
    private Set<MoveablePiece> hackerPieces;
    private Set<MoveablePiece> defenderPieces;
    private DefenderObjective objective;


    //	@Requires("boardRadius > 0")
    public Board(int boardRadius) {
        this.BOARD_RADIUS = boardRadius;
        this.generateTiles();
        this.initPieces();
        this.initObstacles();
    }

    /*
     * Generates all the possible tiles within a hexagon configuration for the radius
     * boardRadius. Add them to the nodes collection in this class.
     * To do this, define center of board as (0,0,0) and then find all possible variations of
     * of (x,y,z) where -radius <= x,y,z <= radius
     * @postcondition: A hexagonal shaped board of tiles is generated, with a radius of boardRadius
     */

    //@Requires("this.boardTiles == null")
    private void generateTiles() {
        this.boardTiles = new HashSet<CubeCoordinate>();
        //The centre tile
        CubeCoordinate center = new CubeCoordinate(0, 0, 0);
        this.boardTiles.add(center);

        for (int i = 1; i <= this.BOARD_RADIUS; i++) {
            //generate a hexagonal ring around center at radius i and add to set of all tiles
            this.boardTiles.addAll(MovementHelper.getCoordinatesAtRadius(center, i));
        }
    }

    //@Requires({"this.obstacles == null", "this.defenderPieces == null", "this.hackerPieces == null"})
    private void initPieces() {
        PiecePrototypeManager piecePrototypeManager = PiecePrototypeManager.getSingleton();

        this.defenderPieces = new HashSet<MoveablePiece>();
        this.defenderPieces.add(piecePrototypeManager.createUnpaidInternAt(new CubeCoordinate(0, -1, 1)));
        this.defenderPieces.add(piecePrototypeManager.createUnpaidInternAt(new CubeCoordinate(1, 0, -1)));
        this.defenderPieces.add(piecePrototypeManager.createUnpaidInternAt(new CubeCoordinate(1, -1, 0)));
        this.defenderPieces.add(piecePrototypeManager.createSecurityGuardAt(new CubeCoordinate(-1, 0, 1)));
        this.defenderPieces.add(piecePrototypeManager.createSecurityGuardAt(new CubeCoordinate(-1, 1, 0)));
        this.defenderPieces.add(piecePrototypeManager.createFirewallExpertAt(new CubeCoordinate(0, 1, -1)));

        this.hackerPieces = new HashSet<MoveablePiece>();
        this.hackerPieces.add(piecePrototypeManager.createDDOSPawnAt(new CubeCoordinate(0, 10, -10)));
        this.hackerPieces.add(piecePrototypeManager.createDDOSPawnAt(new CubeCoordinate(0, -10, 10)));
        this.hackerPieces.add(piecePrototypeManager.createDDOSPawnAt(new CubeCoordinate(-10, 10, 0)));
        this.hackerPieces.add(piecePrototypeManager.createDecryptionExpertAt(new CubeCoordinate(10, 0, -10)));
        this.hackerPieces.add(piecePrototypeManager.createDecryptionExpertAt(new CubeCoordinate(10, -10, 0)));
        this.hackerPieces.add(piecePrototypeManager.createHackerAt(new CubeCoordinate(-10, 0, 10)));

        this.objective = new DefenderObjective(new CubeCoordinate(0, 0, 0));
    }

    /**
     * Generates all the obstacles tiles within a existing board hexagon configuration
     * Add them to the HashSet in this class.
     * To do this, iterate through each of the existing board times, increasing a count for each time
     * mod the total number of tiles count with the count and if the result < 15 add the current tiles
     * cube coordinates to the obstacles hashset. Don't add an obstacle if the tile is occupied.
     */
    private void initObstacles() {
        PiecePrototypeManager pieceFactory = PiecePrototypeManager.getSingleton();

        this.obstacles = new HashSet<Obstacle>();
        Set<CubeCoordinate> occupiedTiles = this.getOccupiedTiles();
        int count = 1;
        for (CubeCoordinate c : this.boardTiles) {
            if (this.boardTiles.size() % count < 15 && !occupiedTiles.contains(c)) {
                this.obstacles.add(pieceFactory.createObstacleAt(new CubeCoordinate(c.getX(), c.getY(), c.getZ())));
            }
            count++;
        }
    }

    /**
     * Generates all movablepiece details from the passed int
     *
     * @param id is itterated through a hashset of all pieces
     * @return null if not found
     */
    private MoveablePiece getPieceWithID(int id) {
        Set<MoveablePiece> allPieces = new HashSet<MoveablePiece>(this.hackerPieces);
        allPieces.addAll(this.defenderPieces);

        for (MoveablePiece p : allPieces) {
            if (p.getID() == id) {
                return p;
            }
        }
        return null;
    }

    /**
     * changes the models representation of the current occupied tile for a given piece
     *
     * @param pieceID      outlines the details for the piece to be moved
     * @param nextPosition specifies the new location of the piece
     */
    //@Requires({"(this.getHackerPieceCoordinates().contains(currPosition) || this.getDefenderPieceCoordinates().contains(currPosition))", "(activePlayer == 0 || activePlayer == 1)"})
    public void updatePiecePosition(int pieceID, CubeCoordinate nextPosition) {
        MoveablePiece piece = this.getPieceWithID(pieceID);

        piece.setPosition(nextPosition);
    }


    /**
     * generates a representation of the board, all of its pieces with their current abilities
     *
     * @param selectedPieceID  retrieve the legal moves when a particular piece is selected by the user
     * @param activePlayer     specifies the current players turn
     * @param hasMovedThisTurn false if first time moving this turn, true if second time moving
     * @param isAttackMode     false if in move mode, true if in attack mode
     * @return current state of board and current legal moves/attack for a selected piece
     */
    //@Ensures("old(this).equals(this)")
    public BoardState getBoardState(int selectedPieceID, int activePlayer, boolean hasMovedThisTurn, boolean isAttackMode) {
        Set<CubeCoordinate> legalMoves = this.getLegalMoves(selectedPieceID, activePlayer);

        BoardState boardState = new BoardState(this.objective.getPosition(), this.getBoardTileCoordinates(), this.getObstacleCoordinates(),
                this.hackerPieces, this.defenderPieces, legalMoves, hasMovedThisTurn, isAttackMode);
        return boardState;
    }


    private Set<CubeCoordinate> getBoardTileCoordinates() {
        return this.boardTiles;
    }

    private Set<CubeCoordinate> getObstacleCoordinates() {
        Set<CubeCoordinate> coordList = new HashSet<CubeCoordinate>();

        for (Obstacle o : this.obstacles) {
            coordList.add(o.getPosition());
        }

        return coordList;
    }

    /**
     * Removes coordinates that are off the hex board
     *
     * @param coordList
     * @precondition: coordList is non-null
     * @Requires("coordList != null")
     */
    private void removeInvalidCoordinatesFrom(Set<CubeCoordinate> coordList) {
        coordList.retainAll(this.boardTiles);
    }

    /**
     * @return a set of cube coordinates for all pieces on the board which have a piece residing on it
     */
    private Set<CubeCoordinate> getOccupiedTiles() {
        Set<CubeCoordinate> occupiedTiles = new HashSet<CubeCoordinate>();

        for (GamePiece p : this.obstacles) {
            occupiedTiles.add(p.getPosition());
        }
        for (MoveablePiece p : this.hackerPieces) {
            if (p.getHP() > 0) {
                occupiedTiles.add(p.getPosition());
            }
        }
        for (MoveablePiece p : this.defenderPieces) {
            if (p.getHP() > 0) {
                occupiedTiles.add(p.getPosition());
            }
        }

        occupiedTiles.add(this.objective.getPosition());

        return occupiedTiles;
    }

    //@Ensures("old(this).equals(this)")
    public Set<CubeCoordinate> getLegalMoves(int selectedPieceID, int activePlayer) {
        MoveablePiece unit = this.getPieceWithID(selectedPieceID);
        if (unit == null) {
            return new HashSet<CubeCoordinate>();
        }

        Set<CubeCoordinate> legalMoves;

        Set<CubeCoordinate> occupiedTiles = this.getOccupiedTiles();
        if (!this.isAttackState())
            legalMoves = MovementHelper.getMoves(unit.getPosition(), unit.getMovementRange(), occupiedTiles);
        else
            legalMoves = unit.getLegalAttacks();
        this.removeInvalidCoordinatesFrom(legalMoves);
        return legalMoves;
    }

    public boolean isAttackState() {
        return GameController.getSingleton().isAttackMode();
    }

    /**
     * applies damage to a piece when attacked
     *
     * @param attackingPieceID
     * @param pieceBeingAttackedID
     */
    public void applyAttack(int attackingPieceID, int pieceBeingAttackedID) {
        MoveablePiece attackingPiece = this.getPieceWithID(attackingPieceID);
        MoveablePiece pieceBeingAttacked = this.getPieceWithID(pieceBeingAttackedID);
        pieceBeingAttacked.applyDamageFrom(attackingPiece);
    }

    public void restoreState(PieceDetails pieceBeforeCommand) {
        MoveablePiece pieceAffected = this.getPieceWithID(pieceBeforeCommand.getID());
        pieceAffected.restoreState(pieceBeforeCommand);
    }

    public boolean allDefendersDead() {
        return this.isTeamDead(this.defenderPieces);
    }

    public boolean allHackersDead() {
        return this.isTeamDead(this.hackerPieces);
    }

    private boolean isTeamDead(Set<MoveablePiece> team) {
        for (MoveablePiece p : team) {
            if (p.getHP() > 0) {
                return false;
            }
        }
        return true;
    }

    public boolean baseDestroyed() {
        if (this.objective.isDead()) {
            return true;
        }
        return false;
    }

    public void applyDamageToBase() {
        this.objective.damageBase();
    }

    public void healBase() {
        this.objective.heal();
    }

    public void setStanceForPieceWithID(int selectedPieceID, Stance stanceToChangeTo) {
        MoveablePiece unit = this.getPieceWithID(selectedPieceID);
        unit.setStance(stanceToChangeTo);
    }
}
