package model.board;

import java.util.*;

//import com.google.java.contract.Requires;

import model.board.CubeCoordinate;
import model.pieces.MoveablePiece;

public class MovementHelper {
    public static final int EAST = 0;
    public static final int SOUTHEAST = 1;
    public static final int SOUTHWEST = 2;
    public static final int WEST = 3;
    public static final int NORTHWEST = 4;
    public static final int NORTHEAST = 5;

    /**
     * holds transformation vectors to move cubecoord in each direction. directions are indexed as per above\
     */
    private static final int movementVectors[][] = {{1, -1, 0}, {0, -1, 1}, {-1, 0, 1}, {-1, 1, 0}, {0, 1, -1}, {1, 0, -1}};

    public static CubeCoordinate moveCoordinateInDirection(CubeCoordinate coord, int direction) {
        int newX, newY, newZ;
        int movementVector[] = movementVectors[direction];

        newX = coord.getX() + movementVector[0];
        newY = coord.getY() + movementVector[1];
        newZ = coord.getZ() + movementVector[2];

        return new CubeCoordinate(newX, newY, newZ);
    }

    /**
     * gets the next movement vector when called
     *
     * @param direction
     * @return direction.next
     */
    public static int getNextDirection(int direction) {
        return (direction + 1) % 6;
    }

    /**
     * Generates a set of selected hex tiles from a given center to a radius size
     *
     * @param center     a cube coordinate for center position
     * @param ringRadius distance from center
     * @return Set<CubeCoordinate> hexRing, all coordinates from center to radius and around
     * @precondition: 0 < ringRadius <= this.boardRadius
     * @Requires({"ringRadius > 0", "ringRadius <= this.boardRadius"})
     */
    private static Set<CubeCoordinate> getHexRingAround(CubeCoordinate center, int ringRadius) {
        Set<CubeCoordinate> hexRing = new HashSet<CubeCoordinate>();
        CubeCoordinate currCoords = center;
        int currDirection = EAST;

        //Move starting position out to appropriate radius, following northwest direction
        for (int i = 0; i < ringRadius; i++) {
            currCoords = moveCoordinateInDirection(currCoords, NORTHWEST);
        }

        do {
            for (int i = 0; i < ringRadius; i++) {
                hexRing.add(currCoords);
                currCoords = moveCoordinateInDirection(currCoords, currDirection);
            }
            currDirection = getNextDirection(currDirection);
        } while (currDirection != EAST);

        return hexRing;
    }

    /*
     * Find all nodes which fall within the current node and radius number of tiles away.
     * @precondition: radius > 0
     */
    //@Requires({"center != null", "radius > 0"})
    public static Set<CubeCoordinate> getCoordinatesWithinRadius(CubeCoordinate center, int radius) {
        Set<CubeCoordinate> tilesWithinRadius = new HashSet<CubeCoordinate>();

        //Add all rings of hexes up to the specified radius
        for (int i = 1; i <= radius; i++) {
            tilesWithinRadius.addAll(MovementHelper.getHexRingAround(center, i));
        }

        return tilesWithinRadius;
    }

    /*
     * Find all nodes which fall within the current node and radius number of tiles away.
     * @precondition: radius > 0
     */
    //@Requires({"center != null", "radius > 0"})
    public static Set<CubeCoordinate> getCoordinatesAtRadius(CubeCoordinate center, int radius) {
        Set<CubeCoordinate> tilesAtRadius = new HashSet<CubeCoordinate>();
        tilesAtRadius.addAll(getHexRingAround(center, radius));
        return tilesAtRadius;
    }

    /**
     * @param piecePosition
     * @param movementRange
     * @param occupiedTiles
     * @return
     * @Requires({"piecePosition != null", "occupiedTiles != null", "movementRange > 0"})
     */
    public static Set<CubeCoordinate> getMoves(CubeCoordinate piecePosition, int movementRange, Set<CubeCoordinate> occupiedTiles) {
        Set<CubeCoordinate> unblockedTilesInRange = getCoordinatesWithinRadius(piecePosition, movementRange);

        occupiedTiles.retainAll(unblockedTilesInRange);
        unblockedTilesInRange.removeAll(occupiedTiles);

        Set<CubeCoordinate> legalMoves = bfs(piecePosition, movementRange, unblockedTilesInRange);

        return legalMoves;
    }

    /**
     * performs Breadth first search from a starting position, for a specified range, in a set
     * This method ensures that a piece can move only in its predetermined range, including obstacles which it cannot jump
     *
     * @precondition: startPos is contained in tilesToSearch
     */
    private static Set<CubeCoordinate> bfs(CubeCoordinate startPos, int range, Set<CubeCoordinate> tilesToSearch) {
        HashMap<CubeCoordinate, Integer> distances = new HashMap<CubeCoordinate, Integer>();
        Set<CubeCoordinate> visitedPositions = new HashSet<CubeCoordinate>();
        ArrayDeque<CubeCoordinate> frontier = new ArrayDeque<CubeCoordinate>(); //used as a queue for the breadth first search

        frontier.add(startPos);
        distances.put(startPos, 0);

        int currDistance = 0;
        CubeCoordinate currPos = startPos;

        while (!frontier.isEmpty()) {
            currPos = frontier.remove();
            currDistance = distances.get(currPos);

            if (currDistance == range) {
                break;
            }

            int currDirection = MovementHelper.EAST;
            do {
                currDistance = distances.get(currPos);

                CubeCoordinate neighbour = MovementHelper.moveCoordinateInDirection(currPos, currDirection);
                if (tilesToSearch.contains(neighbour) && !visitedPositions.contains(neighbour)) {
                    frontier.add(neighbour);
                    visitedPositions.add(neighbour);
                    distances.put(neighbour, currDistance + 1);
                }

                currDirection = MovementHelper.getNextDirection(currDirection);
            } while (currDirection != MovementHelper.EAST);
        }

        return visitedPositions;
    }

    public static int[][] getMovementVectors() {
        return movementVectors;
    }
}
