package model.board;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

import model.pieces.MoveablePiece;

import java.util.HashSet;

public class BoardState implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 2149540215221341488L;
    private Set<CubeCoordinate> boardTiles;
    private Set<CubeCoordinate> obstacles;
    private Set<PieceDetails> hackerPieces;
    private Set<PieceDetails> defenderPieces;
    private Set<CubeCoordinate> legalMoves;
    private Set<CubeCoordinate> attackableCoordinates;
    private CubeCoordinate objectivePosition;

    public BoardState(CubeCoordinate objective, Set<CubeCoordinate> boardTiles, Set<CubeCoordinate> obstacles,
                      Set<MoveablePiece> hackerPieces, Set<MoveablePiece> defenderPieces, Set<CubeCoordinate> legalMoves,
                      boolean hasMovedThisTurn, boolean attackMode) {
        this.boardTiles = boardTiles;
        this.obstacles = obstacles;
        this.hackerPieces = this.generatePieceDetails(hackerPieces);
        this.defenderPieces = this.generatePieceDetails(defenderPieces);
        this.objectivePosition = objective;

        if (attackMode) {
            this.legalMoves = new HashSet<CubeCoordinate>();
            this.attackableCoordinates = legalMoves;
        } else {
            this.attackableCoordinates = new HashSet<CubeCoordinate>();
            if (!hasMovedThisTurn) {
                this.legalMoves = legalMoves;
            } else {
                this.legalMoves = new HashSet<CubeCoordinate>();
            }
        }
    }

    private Set<PieceDetails> generatePieceDetails(Set<MoveablePiece> pieces) {
        Set<PieceDetails> allPieceDetails = new HashSet<PieceDetails>();
        for (MoveablePiece p : pieces) {
            if (p.getHP() > 0)    //Do not tell view about dead pieces. NOTE: The dead pieces will still exist in memory, as this allows us to restore their state later on an undo
            {
                allPieceDetails.add(new PieceDetails(p));
            }
        }

        return allPieceDetails;
    }

    public PieceDetails getDetailsForPieceWithID(int selectedPieceID) {
        Set<PieceDetails> allPieceDetails = new HashSet<PieceDetails>(this.hackerPieces);
        allPieceDetails.addAll(this.defenderPieces);

        for (PieceDetails p : allPieceDetails) {
            if (p.getID() == selectedPieceID) {
                return p;
            }
        }
        return null;    //this should never occur
    }

    public int getPieceIDAt(CubeCoordinate position) {
        Set<PieceDetails> allPieces = new HashSet<PieceDetails>(this.hackerPieces);
        allPieces.addAll(this.defenderPieces);

        for (PieceDetails p : allPieces) {
            if (p.getPosition().equals(position)) {
                return p.getID();
            }
        }

        return -1;
    }

    public Set<CubeCoordinate> getBoardTiles() {
        return Collections.unmodifiableSet(this.boardTiles);
    }

    public Set<CubeCoordinate> getObstacles() {
        return Collections.unmodifiableSet(this.obstacles);
    }

    public Set<PieceDetails> getHackerPieces() {
        return Collections.unmodifiableSet(this.hackerPieces);
    }

    public Set<PieceDetails> getDefenderPieces() {
        return Collections.unmodifiableSet(this.defenderPieces);
    }

    public Set<CubeCoordinate> getHackerPieceCoordinates() {
        return this.extractCoordinatesFrom(this.hackerPieces);
    }

    public Set<CubeCoordinate> getDefenderPieceCoordinates() {
        return this.extractCoordinatesFrom(this.defenderPieces);
    }

    private Set<CubeCoordinate> extractCoordinatesFrom(Set<PieceDetails> pieces) {
        Set<CubeCoordinate> coordList = new HashSet<CubeCoordinate>();

        for (PieceDetails p : pieces) {
            coordList.add(p.getPosition());
        }

        return coordList;
    }

    public Set<CubeCoordinate> getAllPlayerPieceCoordinates() {
        Set<CubeCoordinate> allPieceCoords = new HashSet<>();
        allPieceCoords.addAll(this.extractCoordinatesFrom(this.hackerPieces));
        allPieceCoords.addAll(this.extractCoordinatesFrom(this.defenderPieces));
        return Collections.unmodifiableSet(allPieceCoords);
    }

    public Set<CubeCoordinate> getLegalMoves() {
        return Collections.unmodifiableSet(this.legalMoves);
    }

    public Set<CubeCoordinate> getAttackableCoordinates() {
        return this.attackableCoordinates;
    }

    public CubeCoordinate getObjectivePosition() {
        return this.objectivePosition;
    }
}
