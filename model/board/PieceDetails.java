package model.board;

import java.io.Serializable;

import model.pieces.MoveablePiece;
import model.pieces.states.Stance;
import view.PieceStatus;

public class PieceDetails implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -3856996099544730879L;
    private CubeCoordinate currPosition;
    private int uniqueID;
    private int hp;
    private int strength;
    private int movementRange;
    private int tierLevel;
    private String name;
    private Stance stance;

    public PieceDetails(MoveablePiece piece) {
        this.currPosition = piece.getPosition();
        this.uniqueID = piece.getID();
        this.hp = piece.getHP();
        this.strength = piece.getStrength();
        this.movementRange = piece.getMovementRange();
        this.tierLevel = piece.getTier().getValue();
        this.name = piece.getName();
        this.stance = piece.getStance();
    }

    public CubeCoordinate getPosition() {
        return this.currPosition;
    }

    public int getID() {
        return this.uniqueID;
    }

    public int getStrength() {
        return this.strength;
    }

    public int getMovementRange() {
        return this.movementRange;
    }

    public int getHP() {
        return this.hp;
    }

    public int getTier() {
        return this.tierLevel;
    }

    public String getName() {
        return this.name;
    }

    public Stance getStance() {
        return this.stance;
    }
}
