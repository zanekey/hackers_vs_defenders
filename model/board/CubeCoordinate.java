package model.board;

import java.io.Serializable;

//import com.google.java.contract.*;

/*
 * CLASS INVARIANT: x + y + z == 0
 */
//@Invariant("this.x + this.y + this.z == 0")
public class CubeCoordinate implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8591444085986821543L;
    private int x;
    private int y;
    private int z;


    //@Requires("x + y + z == 0")
    public CubeCoordinate(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    /*
     * Convert the cube coordinates into x,y coordinates. Stored in length 2 array
     */
    public int[] toAxialCoordinates() {
        return new int[]{this.x, this.z};
    }

    public int hashCode() {
        String hashString = "" + this.x + "," + this.y + "," + this.z;
        return hashString.hashCode();
    }

    public boolean equals(Object otherObject) {
        if (otherObject == null) {
            return false;
        }

        CubeCoordinate c = (CubeCoordinate) otherObject;

        return this.x == c.x && this.y == c.y && this.z == c.z;
    }
}
