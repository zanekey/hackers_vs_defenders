package model.pieces.defenders;

import model.board.CubeCoordinate;
import model.pieces.GamePiece;
import model.pieces.MoveablePiece;

public class DefenderObjective extends GamePiece {
    /**
     *
     */
    private static final long serialVersionUID = 9002455967801315950L;
    private int hitsLeft;

    public DefenderObjective(CubeCoordinate coord) {
        super(coord);
        this.hitsLeft = 5;
    }

    /*
     * Calculate and apply damage from another unit. Apply some multiplier based on the tier of
     * current unit and attacking unit (this will be our rock, papers, scissors mechanic). Do not implement
     * for assignment 1. Need to consider what units can deactivate the shield etc.
     */
    public void damageBase() {
        this.hitsLeft--;
    }

    public DefenderObjective clone() {
        DefenderObjective clonedObjective = null;
        try {
            clonedObjective = (DefenderObjective) super.clone();
            clonedObjective.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedObjective;
    }

    public boolean isDead() {
        if (this.hitsLeft > 0) {
            return false;
        }
        return true;
    }

    public void heal() {
        this.hitsLeft++;
    }
}
