package model.pieces.defenders;

import java.util.Set;

import model.board.CubeCoordinate;
import model.pieces.MoveablePiece;
import model.pieces.Tier;
import model.pieces.decorators.*;

public class FirewallExpert extends MoveablePiece {
    /**
     *
     */
    private static final long serialVersionUID = 5108736877828778178L;

    public FirewallExpert(CubeCoordinate coord) {
        super(coord, "FirewallExpert", 2, 60, Tier.HEAVY);

    }

    @Override
    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new XPatternDecorator(this, 2);
        decorator = new EastWestDecorator(decorator, 2);
        return decorator.getAttackableCoordinates();
    }

    public FirewallExpert clone() {
        FirewallExpert clonedFirewallExpert = null;
        try {
            clonedFirewallExpert = (FirewallExpert) super.clone();
            clonedFirewallExpert.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedFirewallExpert;
    }
}