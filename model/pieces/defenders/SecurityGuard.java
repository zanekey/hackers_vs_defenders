package model.pieces.defenders;

import model.board.CubeCoordinate;
import model.board.MovementHelper;
import model.pieces.MoveablePiece;
import model.pieces.Tier;
import model.pieces.decorators.*;

import java.util.Set;

public class SecurityGuard extends MoveablePiece {

    /**
     *
     */
    private static final long serialVersionUID = -7260242936561192808L;

    public SecurityGuard(CubeCoordinate coord) {
        super(coord, "SecurityGuard", 2, 20, Tier.MEDIUM);

    }

    @Override
    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new ZigZagDecorator(this, new int[]{MovementHelper.NORTHEAST, MovementHelper.NORTHWEST, MovementHelper.SOUTHEAST, MovementHelper.SOUTHWEST, MovementHelper.EAST, MovementHelper.WEST}, 3);
        return decorator.getAttackableCoordinates();
    }

    public SecurityGuard clone() {
        SecurityGuard clonedSecurityGuard = null;
        try {
            clonedSecurityGuard = (SecurityGuard) super.clone();
            clonedSecurityGuard.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedSecurityGuard;
    }


}
