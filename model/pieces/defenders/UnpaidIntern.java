package model.pieces.defenders;

import model.pieces.MoveablePiece;
import model.pieces.Tier;
import model.pieces.decorators.AttackDecorator;
import model.pieces.decorators.EastWestDecorator;
import model.pieces.decorators.RingPatternDecorator;
import model.pieces.decorators.XPatternDecorator;
import model.pieces.hackers.DDOSPawn;

import java.util.HashSet;
import java.util.Set;

import model.board.CubeCoordinate;

public class UnpaidIntern extends MoveablePiece {
    /**
     *
     */
    private static final long serialVersionUID = 8410332288333306029L;

    public UnpaidIntern(CubeCoordinate coord) {
        super(coord, "UnpaidIntern", 3, 20, Tier.LIGHT);
    }

    public Set<CubeCoordinate> getAttackableCoordinates() {
        return new HashSet<CubeCoordinate>(); //the base case for our decorator
    }

    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new RingPatternDecorator(this, new int[]{1, 3});
        return decorator.getAttackableCoordinates();
    }

    public UnpaidIntern clone() {
        UnpaidIntern clonedIntern = null;
        try {
            clonedIntern = (UnpaidIntern) super.clone();
            clonedIntern.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedIntern;
    }
}