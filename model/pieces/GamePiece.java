package model.pieces;

//import com.google.java.contract.Requires;

import model.board.CubeCoordinate;

import java.io.Serializable;

public abstract class GamePiece implements Serializable, Cloneable {
    /**
     *
     */
    private static final long serialVersionUID = -4108275667000767351L;
    private static int pieceID = 0;
    private CubeCoordinate position;
    private int uniqueID;

    /**
     * Sets the position of a piece
     *
     * @param coord
     */
    public GamePiece(CubeCoordinate coord) {
        this.position = coord;
        this.uniqueID = GamePiece.pieceID;
    }

    public int getID() {
        return this.uniqueID;
    }

    public CubeCoordinate getPosition() {
        return this.position;
    }

    //@Requires("nextPosition != null")
    public void setPosition(CubeCoordinate nextPosition) {
        this.position = nextPosition;
    }

    protected void assignID() {
        this.uniqueID = this.pieceID;
        GamePiece.pieceID++;
    }
}
