package model.pieces;

import model.pieces.hackers.*;
import model.board.CubeCoordinate;
import model.pieces.defenders.*;

public class PiecePrototypeManager 
{
	/**
	 * 
	 */
    private static PiecePrototypeManager singleton;
    private DDOSPawn ddosPawnPrototype;
    private DecryptionExpert decryptionExpertPrototype;
    private Hacker hackerPrototype;
    private UnpaidIntern unpaidInternPrototype;
    private SecurityGuard securityGuardPrototype;
    private FirewallExpert firewallExpertPrototype;
    private Obstacle obstaclePrototype;

    /**
	 * Initializes all the pieces and obstacles.
	 * 
	 */
    private PiecePrototypeManager() 
    {
        CubeCoordinate defaultCoords = new CubeCoordinate(0, 0, 0);

        this.ddosPawnPrototype = new DDOSPawn(defaultCoords);
        this.decryptionExpertPrototype = new DecryptionExpert(defaultCoords);
        this.hackerPrototype = new Hacker(defaultCoords);
        this.unpaidInternPrototype = new UnpaidIntern(defaultCoords);
        this.securityGuardPrototype = new SecurityGuard(defaultCoords);
        this.firewallExpertPrototype = new FirewallExpert(defaultCoords);
        this.obstaclePrototype = new Obstacle(defaultCoords);
    }

    /**
	 * Creates the piecePrototypeManager if it dosen't exist and returns it.
	 * 
	 * @return PiecePrototypeManager.singleton
	 */
    public static PiecePrototypeManager getSingleton() 
    {
        if (PiecePrototypeManager.singleton == null) 
        {
            PiecePrototypeManager.singleton = new PiecePrototypeManager();
        }
        return PiecePrototypeManager.singleton;
    }

    /**
	 * Creates a DDOSPawn at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createDDOSPawnAt(CubeCoordinate coord) 
    {
        DDOSPawn clone = this.ddosPawnPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a DecryptionExpert at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createDecryptionExpertAt(CubeCoordinate coord) 
    {
        DecryptionExpert clone = this.decryptionExpertPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a Hacker at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createHackerAt(CubeCoordinate coord) 
    {
        Hacker clone = this.hackerPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a UnpaidIntern at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createUnpaidInternAt(CubeCoordinate coord) 
    {
        UnpaidIntern clone = this.unpaidInternPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a SecurityGuard at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createSecurityGuardAt(CubeCoordinate coord) 
    {
        SecurityGuard clone = this.securityGuardPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a FirewallExpert at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public MoveablePiece createFirewallExpertAt(CubeCoordinate coord) 
    {
        FirewallExpert clone = this.firewallExpertPrototype.clone();
        clone.setPosition(coord);
        return clone;
    }

    /**
	 * Creates a Obstacles at given coordinates.
	 * 
	 * @param coord
	 * 
	 * @return clone
	 */
    public Obstacle createObstacleAt(CubeCoordinate coord) 
    {
        Obstacle clone = this.obstaclePrototype.clone();
        clone.setPosition(coord);
        return clone;
    }
}
