package model.pieces.decorators;

import model.board.CubeCoordinate;
import model.board.MovementHelper;

import java.util.HashSet;
import java.util.Set;

public class XPatternDecorator extends AttackDecorator {
    private int range;

    public XPatternDecorator(Attacker attacker, int range) {
        super(attacker);
        this.range = range;
    }

    /**
     * generates an X pattern at south east to north west and north east to south west directions
     *
     * @return
     */
    @Override
    public Set<CubeCoordinate> getAttackableCoordinates() {
        int attackDirections[] = new int[]{MovementHelper.NORTHWEST, MovementHelper.NORTHEAST, MovementHelper.SOUTHEAST, MovementHelper.SOUTHWEST};

        Set<CubeCoordinate> baseCoords = this.attacker.getAttackableCoordinates();

        Set<CubeCoordinate> additionalCoords = new HashSet<CubeCoordinate>();
        for (int i = 0; i < attackDirections.length; i++) {
            CubeCoordinate origin = this.getPosition();
            for (int k = 0; k < range; k++) {
                origin = MovementHelper.moveCoordinateInDirection(origin, attackDirections[i]);
                additionalCoords.add(origin);
            }
        }
        baseCoords.addAll(additionalCoords);
        return baseCoords;
    }
}
