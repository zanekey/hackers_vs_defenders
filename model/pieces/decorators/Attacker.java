package model.pieces.decorators;

import model.board.CubeCoordinate;

import java.util.*;

public interface Attacker {
    public Set<CubeCoordinate> getAttackableCoordinates();

    public CubeCoordinate getPosition();
}
