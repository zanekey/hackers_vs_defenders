package model.pieces.decorators;

import model.board.CubeCoordinate;
import model.board.MovementHelper;

import java.util.HashSet;
import java.util.Set;

public class ZigZagDecorator extends AttackDecorator {
    private int[] directions;
    private int range;

    public ZigZagDecorator(Attacker attacker, int[] directions, int range) {
        super(attacker);
        this.range = range;
        this.directions = directions;
    }

    /**
     * generates a zigzag style pattern for a specified number of directions >1, <6 at a range >0
     *
     * @return
     */
    @Override
    public Set<CubeCoordinate> getAttackableCoordinates() {
        Set<CubeCoordinate> baseCoords = this.attacker.getAttackableCoordinates();

        for (int i = 0; i < directions.length; i++) {
            CubeCoordinate origin = this.getPosition();
            int currDirection = directions[i];
            for (int j = 0; j < range; j++) {
                origin = MovementHelper.moveCoordinateInDirection(origin, currDirection);
                baseCoords.add(origin);

                if (j % 2 == 0) {
                    if (currDirection == this.directions[i]) {
                        currDirection = MovementHelper.getNextDirection(currDirection);
                    } else {
                        currDirection = this.directions[i];
                    }
                }
            }
        }


        return baseCoords;
    }
}