package model.pieces.decorators;

import model.board.CubeCoordinate;
import model.pieces.MoveablePiece;
import model.pieces.Tier;

import java.util.Set;

public abstract class AttackDecorator implements Attacker {
    protected Attacker attacker;

    public AttackDecorator(Attacker attacker) {
        this.attacker = attacker;
    }

    @Override
    public CubeCoordinate getPosition() {
        return attacker.getPosition();
    }

    @Override
    public abstract Set<CubeCoordinate> getAttackableCoordinates();
}

