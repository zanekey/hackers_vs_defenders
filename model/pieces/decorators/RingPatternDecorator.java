package model.pieces.decorators;

import model.board.CubeCoordinate;
import model.board.MovementHelper;

import java.util.HashSet;
import java.util.Set;

public class RingPatternDecorator extends AttackDecorator {
    private int[] range;

    public RingPatternDecorator(Attacker attacker, int[] range) {
        super(attacker);
        this.range = range;
    }

    /**
     * generates a circle of attackable coordinates at a specified range[] > 0
     *
     * @return
     */
    @Override
    public Set<CubeCoordinate> getAttackableCoordinates() {
        Set<CubeCoordinate> baseCoords = this.attacker.getAttackableCoordinates();

        CubeCoordinate origin = this.getPosition();
        for (int k = 0; k < range.length; k++) {
            baseCoords.addAll(MovementHelper.getCoordinatesAtRadius(origin, range[k]));
        }
        return baseCoords;
    }
}
