package model.pieces.states;

import java.io.Serializable;

import model.pieces.MoveablePiece;

public class OffensiveStance implements Stance, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9086308452782520031L;
    private static Stance instance = null;

    @Override
    public int getMovementRangeModifier(MoveablePiece unit) {
        return 2; //better movement range on offense
    }

    @Override
    public double getStrengthModifier(MoveablePiece unit) {
        return 1.5;
    }

    @Override
    public int getDamageForAttack(MoveablePiece defender, MoveablePiece attacker) {
        double defenderTierValue = defender.getTier().getValue();
        double attackerTierValue = attacker.getTier().getValue();

        double damage = attacker.getStrength() * (attackerTierValue / defenderTierValue);
        damage = Math.round(damage);
        return (int) (2 * damage); //take double damage in offensive mode
    }

    public static Stance getInstance() {
        if (OffensiveStance.instance == null) {
            OffensiveStance.instance = new OffensiveStance();
        }
        return OffensiveStance.instance;
    }
}
