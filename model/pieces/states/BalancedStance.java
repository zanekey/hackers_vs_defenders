package model.pieces.states;

import java.io.Serializable;

import model.pieces.MoveablePiece;

public class BalancedStance implements Stance, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1139569879803776864L;
    private static Stance instance = null;

    @Override
    public int getMovementRangeModifier(MoveablePiece unit) {
        return 0;
    }

    @Override
    public double getStrengthModifier(MoveablePiece unit) {
        return 1.0; //no change when in balanced stance
    }

    @Override
    public int getDamageForAttack(MoveablePiece defender, MoveablePiece attacker) {
        double defenderTierValue = defender.getTier().getValue();
        double attackerTierValue = attacker.getTier().getValue();

        double damage = attacker.getStrength() * (attackerTierValue / defenderTierValue);
        damage = Math.round(damage);
        defender.setStance(OffensiveStance.getInstance());
        return (int) damage;
    }

    public static Stance getInstance() {
        if (BalancedStance.instance == null) {
            BalancedStance.instance = new BalancedStance();
        }
        return BalancedStance.instance;
    }
}
