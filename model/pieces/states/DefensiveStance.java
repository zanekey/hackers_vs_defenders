package model.pieces.states;

import java.io.Serializable;

import model.pieces.MoveablePiece;

public class DefensiveStance implements Stance, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5969111945897928425L;
    private static Stance instance = null;


    @Override
    public int getMovementRangeModifier(MoveablePiece unit) {
        return -1; //movement penalty of 1 for defending
    }

    @Override
    public double getStrengthModifier(MoveablePiece unit) {
        return 0.5; //half damage when defending
    }

    @Override
    public int getDamageForAttack(MoveablePiece defender, MoveablePiece attacker) {
        double defenderTierValue = defender.getTier().getValue();
        double attackerTierValue = attacker.getTier().getValue();

        double damage = attacker.getStrength() * (attackerTierValue / defenderTierValue);
        damage = Math.round(damage);
        defender.setStance(BalancedStance.getInstance());
        return (int) (0.5 * damage);    //take half damage in defensive mode
    }

    public static Stance getInstance() {
        if (DefensiveStance.instance == null) {
            DefensiveStance.instance = new DefensiveStance();
        }
        return DefensiveStance.instance;
    }

}
