package model.pieces.states;

import java.io.Serializable;

import model.pieces.MoveablePiece;

public interface Stance {
    public int getMovementRangeModifier(MoveablePiece unit);

    public double getStrengthModifier(MoveablePiece unit);

    public int getDamageForAttack(MoveablePiece defender, MoveablePiece attacker);
}
