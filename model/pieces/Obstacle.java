package model.pieces;

import model.board.CubeCoordinate;

public class Obstacle extends GamePiece {
    /**
     *
     */
    private static final long serialVersionUID = -138058480962600131L;

    public Obstacle(CubeCoordinate coord) {
        super(coord);
    }

    public Obstacle clone() {
        Obstacle clonedObstacle = null;
        try {
            clonedObstacle = (Obstacle) super.clone();
        } catch (CloneNotSupportedException e) {

        }
        return clonedObstacle;
    }
}
