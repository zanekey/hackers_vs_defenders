package model.pieces.hackers;

import model.board.MovementHelper;
import model.pieces.MoveablePiece;
import model.pieces.Tier;

import java.util.Set;

import model.board.CubeCoordinate;
import model.pieces.decorators.*;

public class DecryptionExpert extends MoveablePiece {
    /**
     *
     */
    private static final long serialVersionUID = 3988079730826094466L;

    public DecryptionExpert(CubeCoordinate coord) {
        super(coord, "DecryptionExpert", 2, 40, Tier.MEDIUM);
    }


    @Override
    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new ZigZagDecorator(this, new int[]{MovementHelper.NORTHEAST, MovementHelper.NORTHWEST, MovementHelper.SOUTHEAST, MovementHelper.SOUTHWEST}, 3);
        decorator = new RingPatternDecorator(decorator, new int[]{3});
        return decorator.getAttackableCoordinates();
    }

    public DecryptionExpert clone() {
        DecryptionExpert clonedDecryptionExpert = null;
        try {
            clonedDecryptionExpert = (DecryptionExpert) super.clone();
            clonedDecryptionExpert.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedDecryptionExpert;
    }
}

