package model.pieces.hackers;

import model.pieces.MoveablePiece;
import model.pieces.Tier;

import java.util.Set;

import model.board.CubeCoordinate;
import model.pieces.decorators.*;

public class DDOSPawn extends MoveablePiece {
    /**
     *
     */
    private static final long serialVersionUID = 1157931397805184614L;

    public DDOSPawn(CubeCoordinate coord) {
        super(coord, "DDOSPawn", 3, 20, Tier.LIGHT);
    }

    @Override
    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new XPatternDecorator(this, 2);
        decorator = new RingPatternDecorator(decorator, new int[]{2});
        return decorator.getAttackableCoordinates();
    }

    public DDOSPawn clone() {
        DDOSPawn clonedPawn = null;
        try {
            clonedPawn = (DDOSPawn) super.clone();
            clonedPawn.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedPawn;
    }
}