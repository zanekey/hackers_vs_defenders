package model.pieces.hackers;

import model.board.MovementHelper;
import model.pieces.MoveablePiece;
import model.pieces.Tier;

import java.util.Set;

import model.board.CubeCoordinate;
import model.pieces.decorators.AttackDecorator;
import model.pieces.decorators.RingPatternDecorator;
import model.pieces.decorators.ZigZagDecorator;

public class Hacker extends MoveablePiece {

    /**
     *
     */
    private static final long serialVersionUID = -4305722821637236786L;

    public Hacker(CubeCoordinate coord) {
        super(coord, "Hacker", 2, 60, Tier.HEAVY);
    }

    @Override
    public Set<CubeCoordinate> getLegalAttacks() {
        AttackDecorator decorator = new RingPatternDecorator(this, new int[]{1});

        return decorator.getAttackableCoordinates();
    }


    public Hacker clone() {
        Hacker clonedHacker = null;
        try {
            clonedHacker = (Hacker) super.clone();
            clonedHacker.assignID();
        } catch (CloneNotSupportedException e) {

        }
        return clonedHacker;
    }
}