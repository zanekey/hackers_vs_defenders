package model.pieces;

import model.board.CubeCoordinate;
import model.board.PieceDetails;
import model.pieces.decorators.Attacker;
import model.pieces.states.*;

import java.util.HashSet;
import java.util.Set;

public abstract class MoveablePiece extends GamePiece implements Attacker {
    /**
     *
     */
    private static final long serialVersionUID = 7658568008133819667L;
    private Tier tier;
    private final int MOVEMENT_RANGE;
    private final int STRENGTH;
    private int hp;
    private String name;
    private Stance stance;

    /**
     * Sets the initial piece details
     *
     * @param coord
     * @param name
     * @param movementRange
     * @param strength
     * @param tier
     */
    public MoveablePiece(CubeCoordinate coord, String name, int movementRange, int strength, Tier tier) {
        super(coord);
        this.tier = tier;
        this.MOVEMENT_RANGE = movementRange;
        this.STRENGTH = strength;
        this.hp = 100;
        this.name = name;
        this.stance = new BalancedStance();
    }

    public int getMovementRange() {
        return this.MOVEMENT_RANGE + this.stance.getMovementRangeModifier(this);
    }

    public int getStrength() {
        return (int) (this.STRENGTH * this.stance.getStrengthModifier(this));
    }

    public Tier getTier() {
        return this.tier;
    }

    public int getHP() {
        return this.hp;
    }

    public String getName() {
        return name;
    }

    /**
     * Calculates and applies damage to the HP of the attacked piece.
     *
     * @param attacker
     */
    public void applyDamageFrom(MoveablePiece attacker) {
        this.hp -= this.stance.getDamageForAttack(this, attacker);
    }

    public Set<CubeCoordinate> getAttackableCoordinates() {
        return new HashSet<CubeCoordinate>();
    }

    public abstract Set<CubeCoordinate> getLegalAttacks();

    /**
     * Restores the piece HP, Position and stance to the previous state.
     *
     * @param pieceBeforeCommand
     */
    public void restoreState(PieceDetails pieceBeforeCommand) {
        this.hp = pieceBeforeCommand.getHP();
        this.setPosition(pieceBeforeCommand.getPosition());
        this.setStance(pieceBeforeCommand.getStance());
    }

    public void setStance(Stance stance) {
        this.stance = stance;
    }

    public Stance getStance() {
        return this.stance;
    }
}

