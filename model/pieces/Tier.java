package model.pieces;

import java.io.Serializable;

public enum Tier implements Serializable {
    LIGHT(1),
    MEDIUM(2),
    HEAVY(3);

    private int value;

    Tier(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
