package model.commands;

import java.io.Serializable;

import model.ModelController;
import model.board.PieceDetails;

public abstract class PieceCommand implements Command, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 2565163606139726903L;
    private PieceDetails stateBeforeCommand;

    public PieceCommand(PieceDetails stateBeforeCommand) {
        this.stateBeforeCommand = stateBeforeCommand;
    }

    public void undo() {
        ModelController.getSingleton().restoreState(this.stateBeforeCommand);
    }
}
