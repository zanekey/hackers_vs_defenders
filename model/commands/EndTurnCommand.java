package model.commands;

import java.io.Serializable;

import model.ModelController;

public class EndTurnCommand implements Command, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8303911443475920877L;

    @Override
    public void execute() {
        ModelController.getSingleton().swapTurn();
    }

    @Override
    public void undo() {
        ModelController.getSingleton().unwindTurn();
    }
}