package model.commands;

public interface Command {
    public abstract void execute();

    public void undo();
}
