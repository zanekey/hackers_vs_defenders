package model.commands;

import model.ModelController;
import model.board.PieceDetails;

public class AttackCommand extends PieceCommand {
    /**
     *
     */
    private static final long serialVersionUID = 255667407773681189L;
    private int pieceBeingAttacked;
    private int pieceAttacking;

    public AttackCommand(PieceDetails stateBeforeCommand, int pieceAttacking, int pieceBeingAttacked) {
        super(stateBeforeCommand);
        this.pieceBeingAttacked = pieceBeingAttacked;
        this.pieceAttacking = pieceAttacking;
    }

    public void execute() {
        ModelController.getSingleton().applyAttack(this.pieceAttacking, this.pieceBeingAttacked);
    }
}
