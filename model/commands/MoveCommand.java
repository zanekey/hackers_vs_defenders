package model.commands;

import model.ModelController;
import model.board.CubeCoordinate;
import model.board.PieceDetails;

public class MoveCommand extends PieceCommand {
    /**
     *
     */
    private static final long serialVersionUID = -4895841535089904976L;
    private int pieceID;
    private CubeCoordinate moveDestination;

    public MoveCommand(PieceDetails state, int pieceID, CubeCoordinate movePosition) {
        super(state);
        this.moveDestination = movePosition;
        this.pieceID = pieceID;
    }

    public void execute() {
        ModelController.getSingleton().updatePiecePosition(this.pieceID, this.moveDestination);
    }
}
