package model.commands;

import model.ModelController;

public class BaseAttackCommand implements Command {
    public void execute() {
        ModelController.getSingleton().applyDamageToBase();
    }

    public void undo() {
        ModelController.getSingleton().healBase();
    }
}
